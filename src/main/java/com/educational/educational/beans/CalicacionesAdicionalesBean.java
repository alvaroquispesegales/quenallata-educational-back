package com.educational.educational.beans;


import com.educational.educational.dto.CalificacionesAdicionalesDTO;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

public class CalicacionesAdicionalesBean implements Serializable {
    @Getter
    @Setter
    private ResponseBean API;

    @Getter @Setter
    private CalificacionesAdicionalesDTO result;
}
