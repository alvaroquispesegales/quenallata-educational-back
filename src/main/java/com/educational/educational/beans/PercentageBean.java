package com.educational.educational.beans;

import lombok.Getter;
import lombok.Setter;

public class PercentageBean {
    @Getter @Setter
    private Integer total;
    @Getter @Setter
    private Integer downloads;
}
