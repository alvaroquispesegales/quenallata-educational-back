package com.educational.educational.dto;
import lombok.Data;
import java.util.Date;

@Data
public class CalificacionesAdicionalesDTO {

    private Integer calificacionAdicionalId;
    private Integer studentId;
    private Double evaluacionCompetencia20;
    private Double coevaluacion20;
    private Double cuadernoTrabajo30;
    private Double trabajos10;
    private Double examen30;
    private Double promedio;
    private Date fechaRegistro;

}
