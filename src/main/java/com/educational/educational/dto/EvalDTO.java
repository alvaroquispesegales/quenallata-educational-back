package com.educational.educational.dto;

import lombok.Data;

@Data
public class EvalDTO {

    private Integer id;
    private String response;

}
