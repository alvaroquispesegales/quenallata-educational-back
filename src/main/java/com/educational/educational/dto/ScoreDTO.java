package com.educational.educational.dto;

import lombok.Data;

@Data
public class ScoreDTO {
    private Integer score;
    private Integer evaluation;
}
