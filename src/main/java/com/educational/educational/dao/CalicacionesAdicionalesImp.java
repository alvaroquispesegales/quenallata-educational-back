package com.educational.educational.dao;


import com.educational.educational.dto.CalificacionesAdicionalesDTO;
import com.educational.educational.models.CalificacionesAdicionales;
import com.educational.educational.models.Courses;
import com.educational.educational.models.Materials;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Date;
import java.util.List;

import static java.lang.Integer.parseInt;

@Repository
@Transactional
public class CalicacionesAdicionalesImp implements CalicacionesAdicionalesDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public CalificacionesAdicionalesDTO insertOrUpdateCalificacion(CalificacionesAdicionalesDTO calificacionesAdicionalesDTO) {


        if(calificacionesAdicionalesDTO.getCalificacionAdicionalId()>0){
            CalificacionesAdicionales resultC = entityManager.find(CalificacionesAdicionales.class, calificacionesAdicionalesDTO.getCalificacionAdicionalId());

            if(resultC == null) {
                return null;
            }
            resultC.setEvaluacionCompetencia20(calificacionesAdicionalesDTO.getEvaluacionCompetencia20());
            resultC.setCoevaluacion20(calificacionesAdicionalesDTO.getCoevaluacion20());
            resultC.setCuadernoTrabajo30(calificacionesAdicionalesDTO.getCuadernoTrabajo30());
            resultC.setTrabajos10(calificacionesAdicionalesDTO.getTrabajos10());
            resultC.setExamen30(calificacionesAdicionalesDTO.getExamen30());
            resultC.setPromedio(calificacionesAdicionalesDTO.getPromedio());

            entityManager.flush();


            calificacionesAdicionalesDTO.setCalificacionAdicionalId(resultC.getCalificacionAdicionalId());
            calificacionesAdicionalesDTO.setStudentId(resultC.getStudentId());

            return calificacionesAdicionalesDTO;
        }else{
            CalificacionesAdicionales objInsert = new CalificacionesAdicionales();
            objInsert.setStudentId(calificacionesAdicionalesDTO.getStudentId());
            objInsert.setEvaluacionCompetencia20(calificacionesAdicionalesDTO.getEvaluacionCompetencia20());
            objInsert.setCoevaluacion20(calificacionesAdicionalesDTO.getCoevaluacion20());
            objInsert.setCuadernoTrabajo30(calificacionesAdicionalesDTO.getCuadernoTrabajo30());
            objInsert.setTrabajos10(calificacionesAdicionalesDTO.getTrabajos10());
            objInsert.setExamen30(calificacionesAdicionalesDTO.getExamen30());
            objInsert.setPromedio(calificacionesAdicionalesDTO.getPromedio());
            objInsert.setFechaRegistro(new Date());
            CalificacionesAdicionales newCalificacionAdicional = entityManager.merge(objInsert);
            calificacionesAdicionalesDTO.setCalificacionAdicionalId(  newCalificacionAdicional.getCalificacionAdicionalId());
            return calificacionesAdicionalesDTO;
        }


    }

    @Override
    public boolean eliminarCalificacion(Integer calificacionId) {
        CalificacionesAdicionales result = entityManager.find(CalificacionesAdicionales.class, calificacionId);
        if(result == null) {
            return false;
        }
        result.setStatus(0);
        entityManager.flush();
        return true;
    }

    @Override
    public CalificacionesAdicionalesDTO obtenerCalificacionPorEstudiante(Integer estudianteId) {

        String query = "FROM CalificacionesAdicionales WHERE student_id = :estudianteId AND status = 1";

        List<CalificacionesAdicionales> result = entityManager.createQuery(query, CalificacionesAdicionales.class)
                .setParameter("estudianteId", parseInt(estudianteId+""))
                .getResultList();
        if(result.isEmpty()){
            return null;
        }else{
            CalificacionesAdicionales obj = result.get(0);
            CalificacionesAdicionalesDTO objReturn = new CalificacionesAdicionalesDTO();
            objReturn.setCalificacionAdicionalId(obj.getCalificacionAdicionalId());
            objReturn.setEvaluacionCompetencia20(obj.getEvaluacionCompetencia20());
            objReturn.setCoevaluacion20(obj.getCoevaluacion20());
            objReturn.setCuadernoTrabajo30(obj.getCuadernoTrabajo30());
            objReturn.setTrabajos10(obj.getTrabajos10());
            objReturn.setExamen30(obj.getExamen30());
            objReturn.setPromedio(obj.getPromedio());
            return objReturn;
        }

    }
}
