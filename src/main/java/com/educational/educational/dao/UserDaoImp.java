package com.educational.educational.dao;

import com.educational.educational.models.CalificacionesAdicionales;
import com.educational.educational.models.Users;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

import static java.lang.Integer.parseInt;

@Repository
@Transactional
public class UserDaoImp implements UserDao {

    @PersistenceContext
//  entity for connection to database
    private EntityManager entityManager;

    @Override
    public List<Users> getUsers() {
        String query = "FROM Users";
        List<Users> users = entityManager.createQuery(query, Users.class).getResultList();
        return users;
    }

    @Override
    public Users obtenerUsuarioPorCodigo(String codigo) {
        String query = "FROM Users WHERE code = :code AND status = 1";

        List<Users> result = entityManager.createQuery(query, Users.class)
                .setParameter("code", codigo)
                .getResultList();
        return result.get(0);
    }


}
