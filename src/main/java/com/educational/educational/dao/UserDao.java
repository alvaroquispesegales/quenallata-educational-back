package com.educational.educational.dao;


import com.educational.educational.models.Users;

import java.util.List;

public interface UserDao {

    List<Users> getUsers();

    Users obtenerUsuarioPorCodigo(String codigo);

}
