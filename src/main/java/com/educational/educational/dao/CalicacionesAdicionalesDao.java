package com.educational.educational.dao;

import com.educational.educational.dto.CalificacionesAdicionalesDTO;


public interface CalicacionesAdicionalesDao {
    CalificacionesAdicionalesDTO insertOrUpdateCalificacion(CalificacionesAdicionalesDTO calicacionesAdicionalesDTO);

    boolean eliminarCalificacion(Integer calificacionId);

    CalificacionesAdicionalesDTO obtenerCalificacionPorEstudiante(Integer estudianteId);

}
