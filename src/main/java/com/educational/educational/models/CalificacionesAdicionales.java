package com.educational.educational.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "calicaciones_adicionales")
public class CalificacionesAdicionales {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter @Setter @Column( name = "calificacion_adicional_id" )
    private Integer calificacionAdicionalId;

    @Getter @Setter @Column(name = "student_id")
    private Integer studentId;

    @Getter @Setter @Column(name = "evaluacion_competencia_20")
    private Double evaluacionCompetencia20;

    @Getter @Setter @Column(name = "coevaluacion_20")
    private Double coevaluacion20;

    @Getter @Setter @Column(name = "cuaderno_trabajo_30")
    private Double cuadernoTrabajo30;

    @Getter @Setter @Column(name = "trabajos_10")
    private Double trabajos10;

    @Getter @Setter @Column(name = "examen_30")
    private Double examen30;

    @Getter @Setter @Column(name = "promedio")
    private Double promedio;



    @Getter @Setter @Column(name = "fecha_registro")
    private Date fechaRegistro;

    @Getter @Setter @Column(name = "status")
    private Integer status = 1;

}
