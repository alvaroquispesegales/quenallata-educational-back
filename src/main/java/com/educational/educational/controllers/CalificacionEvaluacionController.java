package com.educational.educational.controllers;

import com.educational.educational.beans.*;
import com.educational.educational.dao.CourseDao;
import com.educational.educational.dao.UserDao;
import com.educational.educational.dto.CalificacionesAdicionalesDTO;
import com.educational.educational.models.CalificacionesAdicionales;
import com.educational.educational.models.Courses;
import com.educational.educational.models.Users;
import com.educational.educational.utils.JWTUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.educational.educational.dao.CalicacionesAdicionalesDao;

import java.util.Date;

import static java.lang.Integer.parseInt;

@CrossOrigin(origins = "*")
@RestController
public class CalificacionEvaluacionController {

    @Autowired
    private CalicacionesAdicionalesDao calicacionesAdicionalesDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private JWTUtil jwtUtil;

    @RequestMapping(value="api/calificacion-adicional", method= RequestMethod.POST)
    public ResponseEntity<CalicacionesAdicionalesBean> crearCalificacionAdicional(
            @RequestHeader(value = "X-token") String token,
            @RequestBody CalificacionesAdicionalesDTO calicacionesAdicionalesDTO) {
        String userID = jwtUtil.getKey(token);

        ResponseBean responseBean = new ResponseBean();
        responseBean.setDate(new Date().toString());
        CalicacionesAdicionalesBean calicacionesAdicionalesBean = new CalicacionesAdicionalesBean();

        if(userID == null) {
            responseBean.setCodeError("401");
            responseBean.setMsgError("Token no valido");
            calicacionesAdicionalesBean.setAPI(responseBean);
            return new ResponseEntity<CalicacionesAdicionalesBean>(calicacionesAdicionalesBean, HttpStatus.FORBIDDEN);

        }else{
            CalificacionesAdicionalesDTO result = calicacionesAdicionalesDao.insertOrUpdateCalificacion(calicacionesAdicionalesDTO);
            responseBean.setCodeError("201");
            responseBean.setMsgError("Evaluaciones creado exitosamente.");
            calicacionesAdicionalesBean.setAPI(responseBean);
            calicacionesAdicionalesBean.setResult(result);
            return new ResponseEntity<CalicacionesAdicionalesBean>(calicacionesAdicionalesBean, HttpStatus.CREATED);
        }
    }
    @RequestMapping(value="api/calificacion-adicional/{calificacionAdicionalId}", method=RequestMethod.DELETE)
    public ResponseEntity<CalicacionesAdicionalesBean> eliminarCalificacionesAdicional(@RequestHeader(value = "X-token") String token, @PathVariable Integer calificacionAdicionalId) {
        ResponseBean responseBean = new ResponseBean();
        responseBean.setDate(new Date().toString());

        CalicacionesAdicionalesBean calicacionesAdicionalesBean = new CalicacionesAdicionalesBean();

        String userID = jwtUtil.getKey(token);

        if(userID == null) {

            responseBean.setCodeError("401");
            responseBean.setMsgError("Token no valido");
            calicacionesAdicionalesBean.setAPI(responseBean);

            return new ResponseEntity<CalicacionesAdicionalesBean>(calicacionesAdicionalesBean, HttpStatus.FORBIDDEN);

        }

        boolean result = calicacionesAdicionalesDao.eliminarCalificacion(calificacionAdicionalId);

        if(!result) {
            responseBean.setCodeError("403");
            responseBean.setMsgError("No autorizado");
            calicacionesAdicionalesBean.setAPI(responseBean);
            return new ResponseEntity<CalicacionesAdicionalesBean>(calicacionesAdicionalesBean, HttpStatus.UNAUTHORIZED);
        }

        responseBean.setCodeError("200");
        responseBean.setMsgError("La Calificación se ha eliminado.");
        calicacionesAdicionalesBean.setAPI(responseBean);

        return new ResponseEntity<CalicacionesAdicionalesBean>(calicacionesAdicionalesBean, HttpStatus.OK);

    }
    @RequestMapping(value = "api/calificacion-adicional/{studentId}", method = RequestMethod.GET)
    public ResponseEntity<CalicacionesAdicionalesBean> obtenerCalificacionesAdicional(@RequestHeader( value = "X-token" ) String token, @PathVariable Integer studentId ) {

        ResponseBean responseBean = new ResponseBean();
        responseBean.setDate(new Date().toString());

        CalicacionesAdicionalesBean calicacionesAdicionalesBean = new CalicacionesAdicionalesBean();



        String userID = jwtUtil.getKey(token);

        if(userID == null) {

            responseBean.setCodeError("401");
            responseBean.setMsgError("Token no valido");
            calicacionesAdicionalesBean.setAPI(responseBean);
            return new ResponseEntity<CalicacionesAdicionalesBean>(calicacionesAdicionalesBean, HttpStatus.FORBIDDEN);

        }

        CalificacionesAdicionalesDTO result = calicacionesAdicionalesDao.obtenerCalificacionPorEstudiante(studentId);

        if( result == null ) {
            responseBean.setCodeError("404");
            responseBean.setMsgError("Calificaciones Adicionales no encontrado");
            calicacionesAdicionalesBean.setAPI(responseBean);

            return new ResponseEntity<CalicacionesAdicionalesBean>(calicacionesAdicionalesBean, HttpStatus.OK);

        }

        responseBean.setCodeError("200");
        responseBean.setMsgError("Calificaciones encontrados");
        calicacionesAdicionalesBean.setAPI(responseBean);
        calicacionesAdicionalesBean.setResult(result);

        return new ResponseEntity<CalicacionesAdicionalesBean>(calicacionesAdicionalesBean, HttpStatus.OK);

    }

}
